<div class="table-responsive-sm">
    <table class="table table-striped" id="months-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Number</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($months as $month)
            <tr>
                <td>{{ $month->name }}</td>
            <td>{{ $month->number }}</td>
                <td>
                    {!! Form::open(['route' => ['months.destroy', $month->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('months.show', [$month->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('months.edit', [$month->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>