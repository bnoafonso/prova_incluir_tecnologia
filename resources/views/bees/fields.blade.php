<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nome:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Qual o nome da abelha']) !!}
</div>

<!-- Species Field -->
<div class="form-group col-sm-6">
    {!! Form::label('species', 'Espécie:') !!}
    {!! Form::text('species', null, ['class' => 'form-control', 'placeholder' => 'Qual a espécie ou nome científico']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <a href="{{route('admin.home')}}"><button type="button" class="botaoCancelar">Cancelar</button></a>
    {!! Form::submit('Cadastrar Abelha', ['class' => 'botaoSubmitCadastroFlor']) !!}
</div>
