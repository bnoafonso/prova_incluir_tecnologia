<div class="table-responsive-sm">
    <table class="table table-striped" id="flowers-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Species</th>
        <th>Description</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($flowers as $flower)
            <tr>
                <td>{{ $flower->name }}</td>
            <td>{{ $flower->species }}</td>
            <td>{{ $flower->description }}</td>
                <td>
                    {!! Form::open(['route' => ['flowers.destroy', $flower->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('flowers.show', [$flower->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('flowers.edit', [$flower->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>