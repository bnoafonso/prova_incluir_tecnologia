@extends('layouts.admin_login')

@section('content')
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Cadastre as flores de acordo com o mês em que elas florescem</strong>
                            </div>
                            <div class="card-body">
                                {{-- <form action="{{route('flowers.store')}}" method="post" enctype="multipart/form-data">
                                    @include('flowers.fields')
                                </form> --}}

                                {!! Form::open(['route' => 'flowers.store', 'enctype' => 'multipart/form-data']) !!}

                                @include('flowers.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
