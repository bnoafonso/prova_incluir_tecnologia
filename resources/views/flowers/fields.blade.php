<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nome:') !!}
    {!! Form::text('name', null, ['class' => 'form-control',  'placeholder' => 'Qual o nome da flor']) !!}
</div>

<!-- Species Field -->
<div class="form-group col-sm-6">
    {!! Form::label('species', 'Espécie:') !!}
    {!! Form::text('species', null, ['class' => 'form-control',  'placeholder' => 'Qual a espécie ou nome científico']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Descrição:') !!}
    {!! Form::text('description', null, ['class' => 'form-control',  'placeholder' => 'Escreva uma breve descrição sobre a flor']) !!}
</div>

<!-- Months Field -->
<div class="form-group col-sm-12">
    {!! Form::label('flower_months', 'Quais  os meses a flor irá florescer:') !!} <br>
    <select class="months related-post form-control" name="months[]" multiple>
        @foreach ($months as $id => $month)
            <option value="{{ $id }}"> <button class="botaoMes">{{$month->name}}</button></option>
        @endforeach
    </select>
</div>

<!-- Bees Field -->
<div class="form-group col-sm-12">
    {!! Form::label('flower_bees', 'Selecione as abelhas que polinizam essa flor:') !!} <br>
    <select class="bees related-post form-control" name="bees[]" multiple>
        @foreach($bees as $id => $bee)
            <option value="{{ $id }}">{{$bee->name}}</option>
        @endforeach
    </select>
</div>

<div class="form-group col-sm-12">
    {!! Form::label('flower_image', 'Selecione uma imagem da flor:') !!} 
    <input type="file" name="image">
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">  
    <a href="{{route('admin.home')}}"><button type="button" class="botaoCancelar">Cancelar</button></a>
    {!! Form::submit('Cadastrar Flor', ['class' => 'botaoSubmitCadastroFlor']) !!}
</div>
