@extends('layouts.admin_login')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-8">
            <div class="titulo">Calendário de Flores</div>
            <div class="descricao">Neste calendario encontra-se diversas flores.<br>
            Podem ser agrupada pelos meses que florescem e/ou <br>  
            pelo tipo de abelha que poliniza a flor. <br><br>
            Selecione as Abelhas
            </div>
        </div>
        <div class="col-lg-4">
            <br>
            <a href="{{ route('flowers.create') }}"><div class="botaoCadastrarFlor"><p class="TextoCadastroFlor">Cadastrar Flor</p></div></a> 
            <br><br><br><br>
            <a href="{{ route('bees.create') }}"><div class="botaoCadastrarFlor"><p class="TextoCadastroFlor">Cadastrar Abelha</p></div></a> 
        </div>
    </div>
    <br><br><br><br><br>
    <br>
    <br><br><br><br><br>
    <div class="row">
        <div class="col">
            <div class="form-group col-sm-12">
                <select class="bees related-post form-control" name="bees[]" multiple>
                    @foreach($bees as $id => $bee)
                        <option value="{{ $id }}">{{$bee->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <br><br>
    <div class="row">
        <!-- Months Field -->
        <div class="col-sm-12">
            {!! Form::label('flower_months', 'Escolha os meses:',  ['class' => 'descricao'])  !!}
        </div>
        <div class="col-sm-12">
            @foreach ($months as $month)
                <input type="button" class="botaoMes" value="{{$month->name}}" onclick="clicarBotao({{$month}},{{$month->number}})">
            @endforeach
        </div>
    </div>
    
    <div class="descricao">
        Flores:
    </div>

    <div class="row">
        <!-- flowers Field -->
        <div class="col-sm-12">
            @foreach( $flowers as $flower )
                {{ $flower->name }}
                <img src="{{ url("flowers/{$flower->image}") }}" alt="{{ $flower->name }}" class="avatar">
            @endforeach
        </div>
    </div>

</div>
@endsection
@section('scripts')
@parent

@endsection