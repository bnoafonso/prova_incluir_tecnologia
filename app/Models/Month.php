<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Month
 * @package App\Models
 * @version January 14, 2021, 8:28 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $flowers
 * @property string $name
 * @property integer $number
 */
class Month extends Model
{

    public $table = 'months';
    
    public $fillable = [
        'name',
        'number'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'number' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function flowers()
    {
        return $this->belongsToMany('App\Models\Flower');
    }

}
