<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Bee
 * @package App\Models
 * @version January 14, 2021, 8:44 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $flowers
 * @property string $name
 * @property string $species
 */
class Bee extends Model
{

    public $table = 'bees';
    



    public $fillable = [
        'name',
        'species'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'species' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function flowers()
    {
        return $this->belongsToMany('App\Models\Flower');
    }
}
