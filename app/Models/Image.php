<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Image
 * @package App\Models
 * @version January 14, 2021, 8:33 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $flowers
 * @property string $file
 * @property string $data
 */
class Image extends Model
{

    public $table = 'images';
    



    public $fillable = [
        'file',
        'data'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'file' => 'string',
        'data' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function flowers()
    {
        return $this->hasMany(\App\Models\Flower::class);
    }
}
