<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Flower
 * @package App\Models
 * @version January 14, 2021, 8:31 pm UTC
 *
 * @property \App\Models\Month $month
 * @property \App\Models\Image $image
 * @property string $name
 * @property string $species
 * @property integer $description
 */
class Flower extends Model
{

    public $table = 'flowers';
    



    public $fillable = [
        'name',
        'species',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'species' => 'string',
        'description' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function image()
    {
        return $this->belongsTo(\App\Models\Image::class);
    }

    public function bees()
    {
        return $this->belongsToMany('App\Models\Bee');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function months()
    {
        return $this->belongsToMany('App\Models\Month');
    }
}
