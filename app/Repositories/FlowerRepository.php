<?php

namespace App\Repositories;

use App\Models\Flower;
use App\Repositories\BaseRepository;

/**
 * Class FlowerRepository
 * @package App\Repositories
 * @version January 14, 2021, 8:31 pm UTC
*/

class FlowerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'species',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Flower::class;
    }
}
