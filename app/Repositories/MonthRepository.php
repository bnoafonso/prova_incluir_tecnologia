<?php

namespace App\Repositories;

use App\Models\Month;
use App\Repositories\BaseRepository;

/**
 * Class MonthRepository
 * @package App\Repositories
 * @version January 14, 2021, 8:28 pm UTC
*/

class MonthRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'number'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Month::class;
    }
}
