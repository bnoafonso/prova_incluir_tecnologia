<?php

namespace App\Repositories;

use App\Models\Bee;
use App\Repositories\BaseRepository;

/**
 * Class BeeRepository
 * @package App\Repositories
 * @version January 14, 2021, 8:44 pm UTC
*/

class BeeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'species'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Bee::class;
    }
}
