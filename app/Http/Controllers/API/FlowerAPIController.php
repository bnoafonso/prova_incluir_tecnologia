<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFlowerAPIRequest;
use App\Http\Requests\API\UpdateFlowerAPIRequest;
use App\Models\Flower;
use App\Repositories\FlowerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\FlowerResource;
use Response;

/**
 * Class FlowerController
 * @package App\Http\Controllers\API
 */

class FlowerAPIController extends AppBaseController
{
    /** @var  FlowerRepository */
    private $flowerRepository;

    public function __construct(FlowerRepository $flowerRepo)
    {
        $this->flowerRepository = $flowerRepo;
    }

    /**
     * Display a listing of the Flower.
     * GET|HEAD /flowers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $flowers = $this->flowerRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(FlowerResource::collection($flowers), 'Flowers retrieved successfully');
    }

    /**
     * Store a newly created Flower in storage.
     * POST /flowers
     *
     * @param CreateFlowerAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateFlowerAPIRequest $request)
    {
        $input = $request->all();

        $flower = $this->flowerRepository->create($input);

        return $this->sendResponse(new FlowerResource($flower), 'Flower saved successfully');
    }

    /**
     * Display the specified Flower.
     * GET|HEAD /flowers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Flower $flower */
        $flower = $this->flowerRepository->find($id);

        if (empty($flower)) {
            return $this->sendError('Flower not found');
        }

        return $this->sendResponse(new FlowerResource($flower), 'Flower retrieved successfully');
    }

    /**
     * Update the specified Flower in storage.
     * PUT/PATCH /flowers/{id}
     *
     * @param int $id
     * @param UpdateFlowerAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFlowerAPIRequest $request)
    {
        $input = $request->all();

        /** @var Flower $flower */
        $flower = $this->flowerRepository->find($id);

        if (empty($flower)) {
            return $this->sendError('Flower not found');
        }

        $flower = $this->flowerRepository->update($input, $id);

        return $this->sendResponse(new FlowerResource($flower), 'Flower updated successfully');
    }

    /**
     * Remove the specified Flower from storage.
     * DELETE /flowers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Flower $flower */
        $flower = $this->flowerRepository->find($id);

        if (empty($flower)) {
            return $this->sendError('Flower not found');
        }

        $flower->delete();

        return $this->sendSuccess('Flower deleted successfully');
    }
}
