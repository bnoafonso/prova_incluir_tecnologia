<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBeeAPIRequest;
use App\Http\Requests\API\UpdateBeeAPIRequest;
use App\Models\Bee;
use App\Repositories\BeeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\BeeResource;
use Response;

/**
 * Class BeeController
 * @package App\Http\Controllers\API
 */

class BeeAPIController extends AppBaseController
{
    /** @var  BeeRepository */
    private $beeRepository;

    public function __construct(BeeRepository $beeRepo)
    {
        $this->beeRepository = $beeRepo;
    }

    /**
     * Display a listing of the Bee.
     * GET|HEAD /bees
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $bees = $this->beeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(BeeResource::collection($bees), 'Bees retrieved successfully');
    }

    /**
     * Store a newly created Bee in storage.
     * POST /bees
     *
     * @param CreateBeeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBeeAPIRequest $request)
    {
        $input = $request->all();

        $bee = $this->beeRepository->create($input);

        return $this->sendResponse(new BeeResource($bee), 'Bee saved successfully');
    }

    /**
     * Display the specified Bee.
     * GET|HEAD /bees/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Bee $bee */
        $bee = $this->beeRepository->find($id);

        if (empty($bee)) {
            return $this->sendError('Bee not found');
        }

        return $this->sendResponse(new BeeResource($bee), 'Bee retrieved successfully');
    }

    /**
     * Update the specified Bee in storage.
     * PUT/PATCH /bees/{id}
     *
     * @param int $id
     * @param UpdateBeeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBeeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Bee $bee */
        $bee = $this->beeRepository->find($id);

        if (empty($bee)) {
            return $this->sendError('Bee not found');
        }

        $bee = $this->beeRepository->update($input, $id);

        return $this->sendResponse(new BeeResource($bee), 'Bee updated successfully');
    }

    /**
     * Remove the specified Bee from storage.
     * DELETE /bees/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Bee $bee */
        $bee = $this->beeRepository->find($id);

        if (empty($bee)) {
            return $this->sendError('Bee not found');
        }

        $bee->delete();

        return $this->sendSuccess('Bee deleted successfully');
    }
}
