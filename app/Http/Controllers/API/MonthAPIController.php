<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMonthAPIRequest;
use App\Http\Requests\API\UpdateMonthAPIRequest;
use App\Models\Month;
use App\Repositories\MonthRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\MonthResource;
use Response;

/**
 * Class MonthController
 * @package App\Http\Controllers\API
 */

class MonthAPIController extends AppBaseController
{
    /** @var  MonthRepository */
    private $monthRepository;

    public function __construct(MonthRepository $monthRepo)
    {
        $this->monthRepository = $monthRepo;
    }

    /**
     * Display a listing of the Month.
     * GET|HEAD /months
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $months = $this->monthRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(MonthResource::collection($months), 'Months retrieved successfully');
    }

    /**
     * Store a newly created Month in storage.
     * POST /months
     *
     * @param CreateMonthAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMonthAPIRequest $request)
    {
        $input = $request->all();

        $month = $this->monthRepository->create($input);

        return $this->sendResponse(new MonthResource($month), 'Month saved successfully');
    }

    /**
     * Display the specified Month.
     * GET|HEAD /months/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Month $month */
        $month = $this->monthRepository->find($id);

        if (empty($month)) {
            return $this->sendError('Month not found');
        }

        return $this->sendResponse(new MonthResource($month), 'Month retrieved successfully');
    }

    /**
     * Update the specified Month in storage.
     * PUT/PATCH /months/{id}
     *
     * @param int $id
     * @param UpdateMonthAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMonthAPIRequest $request)
    {
        $input = $request->all();

        /** @var Month $month */
        $month = $this->monthRepository->find($id);

        if (empty($month)) {
            return $this->sendError('Month not found');
        }

        $month = $this->monthRepository->update($input, $id);

        return $this->sendResponse(new MonthResource($month), 'Month updated successfully');
    }

    /**
     * Remove the specified Month from storage.
     * DELETE /months/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Month $month */
        $month = $this->monthRepository->find($id);

        if (empty($month)) {
            return $this->sendError('Month not found');
        }

        $month->delete();

        return $this->sendSuccess('Month deleted successfully');
    }
}
