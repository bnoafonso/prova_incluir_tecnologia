<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Bee;
use App\Models\Month;
use App\Models\Flower;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bees = Bee::all();
        $months = Month::all();
        $flowers = Flower::all();
        return view('home', compact('bees', 'months', 'flowers'));
    }
}
