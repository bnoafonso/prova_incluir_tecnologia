<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBeeRequest;
use App\Http\Requests\UpdateBeeRequest;
use App\Repositories\BeeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class BeeController extends AppBaseController
{
    /** @var  BeeRepository */
    private $beeRepository;

    public function __construct(BeeRepository $beeRepo)
    {
        $this->beeRepository = $beeRepo;
    }

    /**
     * Display a listing of the Bee.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $bees = $this->beeRepository->paginate(10);

        return view('bees.index')
            ->with('bees', $bees);
    }

    /**
     * Show the form for creating a new Bee.
     *
     * @return Response
     */
    public function create()
    {
        return view('bees.create');
    }

    /**
     * Store a newly created Bee in storage.
     *
     * @param CreateBeeRequest $request
     *
     * @return Response
     */
    public function store(CreateBeeRequest $request)
    {
        $input = $request->all();

        $bee = $this->beeRepository->create($input);

        Flash::success('Bee saved successfully.');

        return redirect(route('admin.home'));
    }

    /**
     * Display the specified Bee.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bee = $this->beeRepository->find($id);

        if (empty($bee)) {
            Flash::error('Bee not found');

            return redirect(route('bees.index'));
        }

        return view('bees.show')->with('bee', $bee);
    }

    /**
     * Show the form for editing the specified Bee.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bee = $this->beeRepository->find($id);

        if (empty($bee)) {
            Flash::error('Bee not found');

            return redirect(route('bees.index'));
        }

        return view('bees.edit')->with('bee', $bee);
    }

    /**
     * Update the specified Bee in storage.
     *
     * @param int $id
     * @param UpdateBeeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBeeRequest $request)
    {
        $bee = $this->beeRepository->find($id);

        if (empty($bee)) {
            Flash::error('Bee not found');

            return redirect(route('bees.index'));
        }

        $bee = $this->beeRepository->update($request->all(), $id);

        Flash::success('Bee updated successfully.');

        return redirect(route('bees.index'));
    }

    /**
     * Remove the specified Bee from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bee = $this->beeRepository->find($id);

        if (empty($bee)) {
            Flash::error('Bee not found');

            return redirect(route('bees.index'));
        }

        $this->beeRepository->delete($id);

        Flash::success('Bee deleted successfully.');

        return redirect(route('bees.index'));
    }
}
