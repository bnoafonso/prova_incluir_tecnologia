<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFlowerRequest;
use App\Http\Requests\UpdateFlowerRequest;
use App\Repositories\FlowerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Month;
use App\Models\Bee;
use Flash;
use Response;

class FlowerController extends AppBaseController
{
    /** @var  FlowerRepository */
    private $flowerRepository;

    public function __construct(FlowerRepository $flowerRepo)
    {
        $this->flowerRepository = $flowerRepo;
    }

    /**
     * Display a listing of the Flower.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $flowers = $this->flowerRepository->paginate(10);

        return view('flowers.index')
            ->with('flowers', $flowers);
    }

    /**
     * Show the form for creating a new Flower.
     *
     * @return Response
     */
    public function create()
    {
        $months = Month::all();
        $bees = Bee::all();
        return view('flowers.create', compact('months', 'bees'));
    }

    /**
     * Store a newly created Flower in storage.
     *
     * @param CreateFlowerRequest $request
     *
     * @return Response
     */
    public function store(CreateFlowerRequest $request)
    {
        $input = $request->all();
        
        $nameFile = null;
 
        // Verifica se informou o arquivo e se é válido
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
             
            // Define um aleatório para o arquivo baseado no timestamps atual
            $name = uniqid(date('HisYmd'));
     
            // Recupera a extensão do arquivo
            $extension = $request->image->extension();
     
            // Define finalmente o nome
            $nameFile = "{$name}.{$extension}";
     
            // Faz o upload:
            $upload = $request->image->storeAs('flowers', $nameFile);
            // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao
     
            // Verifica se NÃO deu certo o upload (Redireciona de volta)
            if ( !$upload ){
                return redirect()->back()->with('error', 'Falha ao fazer upload')->withInput();
            }               

        }

        $bees_id = $input['bees'];

        $months_id = $input['months'];

        $bees = [];

        $months = [];
    
        foreach($bees_id as $b_id){
            $bees[] = Bee::where('id', $b_id)->where('id', '!=', 0)->first();
        }
        
        foreach($months_id as $m_id){
            $months[] = Month::where('id', $m_id)->where('id', '!=', 0)->first();
        }

        $flower = $this->flowerRepository->create($input);

        foreach($bees as $b){
            if($b->id != null){
                $flower->bees()->attach($b->id);
            }
        }

        foreach($months as $m){
            if($m->id != null){
                $flower->months()->attach($m->id);
            }
        }

        Flash::success('Flower saved successfully.');

        return redirect(route('admin.home'));
    }

    /**
     * Display the specified Flower.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $flower = $this->flowerRepository->find($id);

        if (empty($flower)) {
            Flash::error('Flower not found');

            return redirect(route('flowers.index'));
        }

        return view('flowers.show')->with('flower', $flower);
    }

    /**
     * Show the form for editing the specified Flower.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $flower = $this->flowerRepository->find($id);

        if (empty($flower)) {
            Flash::error('Flower not found');

            return redirect(route('flowers.index'));
        }

        return view('flowers.edit')->with('flower', $flower);
    }

    /**
     * Update the specified Flower in storage.
     *
     * @param int $id
     * @param UpdateFlowerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFlowerRequest $request)
    {
        $flower = $this->flowerRepository->find($id);

        if (empty($flower)) {
            Flash::error('Flower not found');

            return redirect(route('flowers.index'));
        }

        $flower = $this->flowerRepository->update($request->all(), $id);

        Flash::success('Flower updated successfully.');

        return redirect(route('flowers.index'));
    }

    /**
     * Remove the specified Flower from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $flower = $this->flowerRepository->find($id);

        if (empty($flower)) {
            Flash::error('Flower not found');

            return redirect(route('flowers.index'));
        }

        $this->flowerRepository->delete($id);

        Flash::success('Flower deleted successfully.');

        return redirect(route('flowers.index'));
    }
}
