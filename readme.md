
Este repositório possui o projeto da prova destinada para uma vaga de estágio na empresa incluir tecnologia.
Foi utilizado o framework Laravel para desenvolvimento do projeto, foram implementadas quase todas as funcionalidades solicitadas no roteiro.
Segue o passo a passo para preparar e iniciar o projeto:

-> criar o banco de dados no mysql com o nome: prova_incluir_tecnologia.

-> é necessário gerar uma chave para o arquivo .env para que seja possível executar o projeto, portanto é preciso executar o comando: php artisan key:generate.

-> em seguida rode o comando: php artisan serve. e o projeto já estará no ar.

-> executar as migrations e o seed(onde se encontram os dados de abelhas solicitados no roteiro e os registros dos meses), executando o seguinte comando: 
php artisan migrate:fresh --seed

-> o projeto já possui também uma parte de gerenciamento de usuários onde é possível atribuir permissões e definir os tipos de usuários.

-> com o usuário Admin que é criado no seeder ja é possível logar no sistema, utilize as seguintes credenciais: Login: admin@admin.com; Senha:password. Isso após a execução do seeder.

