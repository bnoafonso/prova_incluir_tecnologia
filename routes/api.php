<?php

Route::group(['prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {

});


Route::resource('months', 'MonthAPIController');

Route::resource('images', 'ImageAPIController');

Route::resource('flowers', 'FlowerAPIController');

Route::resource('bees', 'BeeAPIController');