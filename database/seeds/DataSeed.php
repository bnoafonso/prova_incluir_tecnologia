<?php

use App\Models\Month;
use App\Models\Bee;
use Illuminate\Database\Seeder;

class DataSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Meses
        Month::create([
            'name' => 'Janeiro',
            'number' => 1,
        ]);
        Month::create([
            'name' => 'Fevereiro',
            'number' => 2,
        ]);
        Month::create([
            'name' => 'Março',
            'number' => 3,
        ]);
        Month::create([
            'name' => 'Abril',
            'number' => 4,
        ]);
        Month::create([
            'name' => 'Maio',
            'number' => 5,
        ]);
        Month::create([
            'name' => 'Junho',
            'number' => 6,
        ]);
        Month::create([
            'name' => 'Julho',
            'number' => 7,
        ]);
        Month::create([
            'name' => 'Agosto',
            'number' => 8,
        ]);
        Month::create([
            'name' => 'Setembro',
            'number' => 9,
        ]);
        Month::create([
            'name' => 'Outubro',
            'number' => 10,
        ]);
        Month::create([
            'name' => 'Novembro',
            'number' => 11,
        ]);
        Month::create([
            'name' => 'Dezembro',
            'number' => 12,
        ]);

        // Abelhas
        Bee::create([
            'name' => 'Uruçu ',
            'species' => 'Melipona scutellaris',
        ]);
        Bee::create([
            'name' => 'Uruçu-Amarela ',
            'species' => 'Melipona rufiventris',
        ]);
        Bee::create([
            'name' => 'Guarupu ',
            'species' => 'Melipona bicolor',
        ]);
        Bee::create([
            'name' => 'Iraí ',
            'species' => 'Nannotrigona testaceicornes',
        ]);
        Bee::create([
            'name' => 'Jataí ',
            'species' => 'Tetragonisca angustula',
        ]);
        Bee::create([
            'name' => 'Jataí-da-Terra ',
            'species' => 'Paratrigona subnuda',
        ]);
        Bee::create([
            'name' => 'Mandaçaia ',
            'species' => 'Melipona mandaçaia',
        ]);
        Bee::create([
            'name' => 'Manduri ',
            'species' => 'Melipona marginata',
        ]);
        Bee::create([
            'name' => 'Tubuna ',
            'species' => 'Scaptotrigona bipunctata',
        ]);
        Bee::create([
            'name' => 'Mirim Droryana',
            'species' => 'Plebeia droryana',
        ]);
        Bee::create([
            'name' => 'Mirim-Guaçu',
            'species' => 'Plebeia remota',
        ]); 
        Bee::create([
            'name' => 'Mirim-Preguiça',
            'species' => 'Friesella Schrottkyi',
        ]);
        Bee::create([
            'name' => 'Lambe-Olhos',
            'species' => 'Leurotrigona muelleri',
        ]);
        Bee::create([
            'name' => 'Borá ',
            'species' => 'Tetragona clavipes',
        ]);
        Bee::create([
            'name' => 'Boca-de-Sapo',
            'species' => 'Partamona helleri',
        ]);
        Bee::create([
            'name' => 'Guira ',
            'species' => 'Geotrigona mombuca',
        ]);
        Bee::create([
            'name' => 'Marmelada Amarela',
            'species' => 'Frieseomelitta varia',
        ]);
        Bee::create([
            'name' => 'Mombucão ',
            'species' => 'Cephalotrigona capitata',
        ]);
        Bee::create([
            'name' => 'Guiruçu ',
            'species' => 'Schwarziana quadripunctata',
        ]);
        Bee::create([
            'name' => 'Tataíra ',
            'species' => 'Oxytrigona tataira tataira',
        ]);
        Bee::create([
            'name' => 'Irapuã ',
            'species' => 'Trigona spinipes',
        ]);
        Bee::create([
            'name' => 'Abelha-Limão',
            'species' => 'Lestrimelitta limao',
        ]);
        Bee::create([
            'name' => 'Bieira ',
            'species' => 'Mourella caerulea',
        ]);
    }
}
