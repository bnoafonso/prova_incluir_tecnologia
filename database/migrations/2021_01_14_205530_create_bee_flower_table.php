<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeeFlowerTable extends Migration
{
  /**
   * Many to Many Pivot Table.
   *
   * This assumes two models. Blog and Tag.
   * Their database tables are "blogs" and "tags" respectively.
   *
   * Not included are any index(), or onDelete() calls.
   * You'll need to look into this yourself.
   *
   * This snippet exists to remind myself how to set it up.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('bee_flower', function(Blueprint $table) {
      $table->id();
      $table->integer('bee_id')->unsigned();
      $table->integer('flower_id')->unsigned();
      $table->foreign('bee_id')->references('id')->on('bees')->onDelete('cascade');
      $table->foreign('flower_id')->references('id')->on('flowers')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   * In this case, we simply drop the table.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropTable('blog_tag');
  }
}